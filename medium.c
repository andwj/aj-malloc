/* Copyright 2019 Andrew Apted.
 * Use of this code is governed by an MIT-style license.
 * See the accompanying "LICENSE.md" file for the full text.
 */

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include "aj_malloc.h"
#include "internal.h"


#define NUM_FLAVORS  3

#define BINS_PER_FLAVOR  12

#define TOTAL_BINS  (NUM_FLAVORS * BINS_PER_FLAVOR)


struct ajmalChunkHeader {
	uint32_t  bin_and_sig;
	uint16_t  offset;
	uint16_t  pages;  // normally 0

	// link into list
	struct ajmalChunkHeader  *next;
	struct ajmalChunkHeader  *prev;
};

#define HIDDEN_HEADER_SIZE  8


struct ajmalBin {
	size_t  obj_size;

	// free chunks
	// NOTE: we must keep this list sorted by chunk address.
	struct ajmalChunkHeader  *head;
	struct ajmalChunkHeader  *tail;
};

static struct ajmalBin ajmal_bins[TOTAL_BINS];


static void ajmal_init_bins(void) {
	size_t sizes[3] = { 72, 92, 120 };

	int bin, f;
	for (bin = 0 ; bin < TOTAL_BINS ; bin += 3) {
		for (f = 0 ; f < NUM_FLAVORS ; f++) {
			ajmal_bins[bin+f].obj_size = sizes[f];

			sizes[f] = sizes[f] * 2 + HIDDEN_HEADER_SIZE;
		}
	}
}


static int ajmal_can_merge(struct ajmalChunkHeader *A, struct ajmalChunkHeader *B,
							uintptr_t chunk_size) {
	if (A >= B)
		return 0;

	// can only merge two buddies.
	// [ this check also eliminates the case of B being the start
	//   of an OS allocation ]
	if ((A->offset & 1) || B->offset != (A->offset + 1))
		return 0;

	// require B to directly follow A in memory
	uintptr_t a2 = (uintptr_t)A;
	uintptr_t b2 = (uintptr_t)B;

	return (b2 == (a2 + chunk_size));
}


static int ajmal_size_to_bin(size_t size) {
	int bin;
	for (bin = 0 ; bin < TOTAL_BINS ; bin++) {
		if (size <= ajmal_bins[bin].obj_size) {
			return bin;
		}
	}

	fprintf(stderr, "PANIC: aj-malloc internal error: no bin for %d\n", (int)size);
	abort();

	// not reached
	return -1;
}


static void ajmal_unlink_chunk(int bin, struct ajmalChunkHeader *hdr) {
	if (hdr->prev != NULL)
		hdr->prev->next = hdr->next;
	else
		ajmal_bins[bin].head = hdr->next;

	if (hdr->next != NULL)
		hdr->next->prev = hdr->prev;
	else
		ajmal_bins[bin].tail = hdr->prev;

	// debugging aid: clear the signature
	hdr->bin_and_sig &= 0xFF;
}


// this adds a large free chunk, allocated by the OS.
// the destination bin will often be larger than the wanted bin,
// which gets split into smaller chunks by other code.
// returns 1 if successful, 0 if failed.
static int ajmal_populate_for_bin(int want_bin) {
	int bin = want_bin;
	while (bin < 29)
		bin += 3;

	size_t chunk_size = ajmal_bins[bin].obj_size + HIDDEN_HEADER_SIZE;

	uint32_t os_pages = (chunk_size + 4095) / 4096;

	void *p = ajmal_os_get_pages(os_pages);
	if (p == NULL)
		return 0;

	struct ajmalChunkHeader *hdr = (struct ajmalChunkHeader *)p;

	hdr->bin_and_sig = (bin & 0xFF) | SIGNAT_FREE_CHUNK;
	hdr->offset      = 0;
	hdr->pages       = os_pages;

	// link it in (we assume the bin is empty!)
	hdr->next = hdr->prev = NULL;

	ajmal_bins[bin].head = hdr;
	ajmal_bins[bin].tail = hdr;

	ajmal_stats.medium_in_use += os_pages * 4096;

	return 1;  // ok
}


// split chunk from 'bin' so that result matches 'want_bin'.
// when bin == want_bin, no actual splitting occurs.
//
// requisite: bin >= want_bin, and all bins in-between are empty,
//            (when bin > want_bin, want_bin must be empty too).
//
// the chunk in 'hdr' is not in any list (caller unlinked it).
// the result chunk (if any) is marked as allocated.
static void * ajmal_split_chunk(int bin, struct ajmalChunkHeader *hdr, int want_bin) {
	uintptr_t p2 = (uintptr_t)hdr;

	while (bin > want_bin) {
		// chunk is too big, must split it.
		// we keep the first half, put second half into current bin.
		bin -= 3;

		ajmal_stats.medium_splits++;

		// set up the chunk in second half
		uintptr_t split_size = ajmal_bins[bin].obj_size + HIDDEN_HEADER_SIZE;
		uintptr_t p3 = p2 + split_size;

		struct ajmalChunkHeader *split = (struct ajmalChunkHeader *)p3;

		split->bin_and_sig = (bin & 0xFF) + SIGNAT_FREE_CHUNK;
		split->pages       = 0;

		// fix the offsets (vital for the buddy system)
		hdr->offset   = hdr->offset * 2;
		split->offset = hdr->offset + 1;

		// link it in (we assume the bin is empty!)
		split->next = split->prev = NULL;

		ajmal_bins[bin].head = split;
		ajmal_bins[bin].tail = split;
	}

	// update signature: mark as used
	hdr->bin_and_sig = (bin & 0xFF) | SIGNAT_USED_CHUNK;

	// return a pointer to usable part of chunk
	return (void *)(p2 + HIDDEN_HEADER_SIZE);
}


static void * ajmal_raw_alloc_medium(int want_bin) {
	// find a bin (of same flavor!) with a free chunk.
	// if there are none, that is ok, caller will create some more.

	int bin;
	for (bin = want_bin ; bin < TOTAL_BINS ; bin += 3) {
		if (ajmal_bins[bin].head != NULL) {
			struct ajmalChunkHeader *hdr = ajmal_bins[bin].head;

			// remove head chunk from the bin
			ajmal_unlink_chunk(bin, hdr);

			// handle chunks that are larger than what we want
			return ajmal_split_chunk(bin, hdr, want_bin);
		}
	}

	return NULL;
}


void * ajmal_alloc_medium(size_t size) {
	if (ajmal_bins[0].obj_size == 0)
		ajmal_init_bins();

	int bin = ajmal_size_to_bin(size);

	// first, see if we can use/split an existing free chunk
	void *p = ajmal_raw_alloc_medium(bin);
	if (p != NULL) {
		goto done;
	}

	// there were no usable chunks, hence need to allocate from OS
	if (0 == ajmal_populate_for_bin(bin))
		return NULL;

	// this should succeed now
	p = ajmal_raw_alloc_medium(bin);
	if (p == NULL) {
		fprintf(stderr, "PANIC: aj_alloc internal failure (medium)\n");
		abort();
	}

done:
	ajmal_stats.medium_allocs++;
	ajmal_stats.medium_actual += size;

	return p;
}


void ajmal_free_medium(size_t size, void *p) {
	if (ajmal_bins[0].obj_size == 0) {
		fprintf(stderr, "PANIC: aj_free -- free without alloc (medium)\n");
		abort();
	}

	int bin = ajmal_size_to_bin(size);

	ajmal_stats.medium_frees++;
	ajmal_stats.medium_actual -= size;

	// get base of chunk
	uintptr_t p2 = (uintptr_t)p - HIDDEN_HEADER_SIZE;

	struct ajmalChunkHeader *hdr = (struct ajmalChunkHeader *)p2;

	// verify the signature
	uint32_t want_sig = (bin & 0xFF) | SIGNAT_USED_CHUNK;

	if ((hdr->bin_and_sig & ~0xFF) == SIGNAT_FREE_CHUNK) {
		fprintf(stderr, "PANIC: aj_free -- double free detected (medium)\n");
		abort();
	}
	if (hdr->bin_and_sig != want_sig) {
		fprintf(stderr, "PANIC: aj_free with wrong size or addr or mem corruption\n");
        abort();
	}

	// the tricky bit: add this chunk back into the bin *BUT*
	// detect when we can merge it with a buddy chunk.
	// we must also keep the chunk lists sorted.
	struct ajmalChunkHeader *other;

repeat:
	/* dumb compiler needs this: */ { }

	int merge_ok = (bin + 3) < TOTAL_BINS;
	uintptr_t chunk_size = ajmal_bins[bin].obj_size + HIDDEN_HEADER_SIZE;

	// find first chunk whose address is > than current
	for (other = ajmal_bins[bin].head ; other != NULL ; other = other->next) {
		if (merge_ok && ajmal_can_merge(hdr, other, chunk_size)) {
			ajmal_stats.medium_merges++;
			ajmal_unlink_chunk(bin, other);

			// fix the offset (it will always be even)
			hdr->offset /= 2;

			bin += 3;
			goto repeat;
		}

		if (merge_ok && ajmal_can_merge(other, hdr, chunk_size)) {
			ajmal_stats.medium_merges++;
			ajmal_unlink_chunk(bin, other);
			hdr = other;

			// fix the offset (it will always be even)
			hdr->offset /= 2;

			bin += 3;
			goto repeat;
		}

#if 1
		// some sanity checks
		if (other == hdr ||
			(other->next && other->next <= other) ||
			(other->prev && other->prev >= other)) {

			fprintf(stderr, "PANIC: aj_free internal snafu (medium)\n");
			abort();
		}
#endif

		// can we link our chunk before the other?
		if (hdr < other) {
			hdr->next = other;
			hdr->prev = other->prev;

			if (hdr->prev != NULL)
				hdr->prev->next = hdr;
			else
				ajmal_bins[bin].head = hdr;

			other->prev = hdr;
			goto done;
		}
	}

	// our chunk address is > all in list (or list is empty),
	// so simply add the chunk to the end of the list.

	hdr->next = NULL;
	hdr->prev = ajmal_bins[bin].tail;

	if (hdr->prev != NULL)
		hdr->prev->next = hdr;
	else
		ajmal_bins[bin].head = hdr;

	ajmal_bins[bin].tail = hdr;

done:
	// set final bin and mark as free
	hdr->bin_and_sig = (bin & 0xFF) | SIGNAT_FREE_CHUNK;
}


#if 0  // DEBUG ONLY
void aj_print_medium_bins(void) {
	printf("Medium bin chunk counts:\n");

	int bin;
	for (bin = 0 ; bin < TOTAL_BINS ; bin++) {
		int count = 0;

		struct ajmalChunkHeader *hdr;
		for (hdr = ajmal_bins[bin].head ; hdr != NULL ; hdr = hdr->prev) {
			count++;
		}

		printf("   bin size %d has %d chunks\n", (int)ajmal_bins[bin].obj_size, count);
	}
}
#endif
