/* Copyright 2019 Andrew Apted.
 * Use of this code is governed by an MIT-style license.
 * See the accompanying "LICENSE.md" file for the full text.
 */

#include <stdlib.h>
#include <stdio.h>

#include "aj_malloc.h"
#include "internal.h"


struct ajmalStatistics ajmal_stats;


void * aj_alloc(size_t size) {
	if (size == 0)
		return NULL;

	if (size <= SMALL_OBJ_SIZE) {
		return ajmal_alloc_small(size);
	}

	if (size <= LARGE_OBJ_SIZE) {
		return ajmal_alloc_medium(size);
	}

	// large object: allocate directly from OS

	ajmal_stats.large_allocs++;

	// determine number of pages (rounding up)
	size_t count = (size + 4095) / 4096;

	void * p = ajmal_os_get_pages(count);

	if (p != NULL) {
		ajmal_stats.large_in_use += (count * 4096);
		ajmal_stats.large_actual += size;
	}

	return p;
}


void aj_free(size_t size, void *p) {
	if (p == NULL || size == 0)
		return;

	if (size <= SMALL_OBJ_SIZE) {
		ajmal_free_small(size, p);
		return;
	}

	if (size <= LARGE_OBJ_SIZE) {
		ajmal_free_medium(size, p);
		return;
	}

	// large object: give back to the OS

	ajmal_stats.large_frees++;

	// determine number of pages (rounding up)
	size_t count = (size + 4095) / 4096;

	// for stats, we assume the free succeeds
	ajmal_stats.large_in_use -= count * 4096;
	ajmal_stats.large_actual -= size;

	ajmal_os_free_pages(count, p);
}


void aj_print_mem_stats(void) {
	printf("\n");
	printf("aj-malloc statistics\n");
	printf("~~~~~~~~~~~~~~~~~~~~\n");

	/* page stats */

	ajmal_os_print_page_size();

	printf("Pages currently in use: %lu\n", ajmal_stats.used_pages);

	printf("Total page allocations: %lu (%lu errors)\n",
		ajmal_stats.total_page_allocs, ajmal_stats.page_alloc_errors);

	printf("Total page frees: %lu (%lu errors)\n",
		ajmal_stats.total_page_frees, ajmal_stats.page_free_errors);

	printf("\n");

	/* small obj stats */

	printf("Small allocations: %lu (%lu frees)\n",
		ajmal_stats.small_allocs, ajmal_stats.small_frees);

	printf("      bytes in use: %lu (%lu requested) efficiency: %1.1f%%\n",
		ajmal_stats.small_in_use, ajmal_stats.small_actual,
		(ajmal_stats.small_in_use == 0) ? 0.0 :
		100.0 * (double)ajmal_stats.small_actual / (double)ajmal_stats.small_in_use);

	/* medium obj stats */

	printf("Medium allocations: %lu (%lu frees)\n",
		ajmal_stats.medium_allocs, ajmal_stats.medium_frees);

	printf("       bytes in use: %lu (%lu requested) efficiency: %1.1f%%\n",
		ajmal_stats.medium_in_use, ajmal_stats.medium_actual,
		(ajmal_stats.medium_in_use == 0) ? 0.0 :
		100.0 * (double)ajmal_stats.medium_actual / (double)ajmal_stats.medium_in_use);

	printf("       chunks split: %lu, merged: %lu\n",
		ajmal_stats.medium_splits, ajmal_stats.medium_merges);

	/* large obj stats */

	printf("Large allocations: %lu (%lu frees)\n",
		ajmal_stats.large_allocs, ajmal_stats.large_frees);

	printf("      bytes in use: %lu (%lu requested) efficiency: %1.1f%%\n",
		ajmal_stats.large_in_use, ajmal_stats.large_actual,
		(ajmal_stats.large_in_use == 0) ? 0.0 :
		100.0 * (double)ajmal_stats.large_actual / (double)ajmal_stats.large_in_use);
}
