/* Copyright 2019 Andrew Apted.
 * Use of this code is governed by an MIT-style license.
 * See the accompanying "LICENSE.md" file for the full text.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

// this must be *after* the standard headers
#include "aj_malloc.h"

/* libc replacements */

void * ajmal_c_malloc(size_t size) {
	if (size == 0) {
		return NULL;
	}
	if (size >= 0x80000000) {
		fprintf(stderr, "PANIC: ajmal_c_malloc with huge size\n");
		abort();
	}

	// allocate 4 extra bytes so we can stored the size
	void * buf = aj_alloc(size + 4);

	if (buf == NULL) {
		errno = ENOMEM;
		return NULL;
	}

	unsigned int * buf2 = (unsigned int *)buf;

	// store size at start of buffer
	buf2[0] = (unsigned int)size;

	// return memory after the size
	return buf2 + 1;
}


void ajmal_c_free(void *p) {
	if (p == NULL) {
		return;
	}

	// get size and original buffer pointer
	unsigned int * buf2 = (unsigned int *)p;
	buf2--;

	size_t size = buf2[0];

	aj_free(size + 4, buf2);
}


void * ajmal_c_calloc(size_t n, size_t size) {
	if (n == 0 || size == 0) {
		return NULL;
	}

	size_t total = n * size;

	if (total >= 0x80000000 || (total / size) != n) {
		fprintf(stderr, "PANIC: ajmal_c_calloc with huge size\n");
		abort();
	}

	void *p = ajmal_c_malloc(total);
	if (p == NULL)
		return NULL;  // errno already set

	memset(p, 0, total);

	return p;
}


void * ajmal_c_realloc(void *p, size_t new_size) {
	if (p == NULL) {
		return ajmal_c_malloc(new_size);
	}

	if (new_size == 0) {
		ajmal_c_free(p);
		return NULL;
	}

	// get size and original buffer pointer
	unsigned int * buf2 = (unsigned int *)p;
	buf2--;

	size_t old_size = buf2[0];

	// no-op when the new size is <= old size
	// [ this could potentially leak a massive chunk of memory, but
	//   it does not matter since this libc emulation stuff is only
	//   here to allow testing our allocator in a real program. ]
	if (new_size <= old_size) {
		return p;
	}

	void * new_p = ajmal_c_malloc(new_size);
	if (new_p == NULL) {
		return NULL;  // errno already set
	}

	memcpy(new_p, p, old_size);

	aj_free(old_size + 4, buf2);

	return new_p;
}


char * ajmal_c_strdup(const char *p) {
	size_t len = strlen(p);

	// note "len+1" to include the trailing NUL byte

	char *new_p = (char *)ajmal_c_malloc(len+1);
	if (new_p == NULL)
		return NULL;  // errno already set

	memcpy(new_p, p, len+1);

	return new_p;
}


char * ajmal_c_strndup(const char *p, size_t n) {
	size_t len = strlen(p);
	if (len > n)
		len = n;

	// note "len+1" to include the trailing NUL byte

	char *new_p = (char *)ajmal_c_malloc(len+1);
	if (new_p == NULL)
		return NULL;  // errno already set

	if (len > 0)
		memcpy(new_p, p, len);

	new_p[len] = 0;

	return new_p;
}


// TESTING CRUD.
// fall through to standard libc malloc() and free(), just to
// check that the emulated functions are working properly.
#if 0
#undef malloc
#undef free

void * aj_alloc(size_t size) {
	return (malloc)(size);
}

void aj_free (size_t size, void *p) {
	(void)size;
	return (free)(p);
}

void aj_print_mem_stats(void) {
}
#endif
