/* Copyright 2019 Andrew Apted.
 * Use of this code is governed by an MIT-style license.
 * See the accompanying "LICENSE.md" file for the full text.
 */

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include "aj_malloc.h"
#include "internal.h"


#define NUM_CATEGORIES  12


struct ajmalSmallHeader {
	uint32_t  cat_and_sig;
	uint32_t  free_count;

	struct ajmalSmallHeader  *next;
	struct ajmalSmallHeader  *prev;
};

#define SMALL_HEADER_SIZE  sizeof(struct ajmalSmallHeader)


struct ajmalCategory {
	size_t  obj_size;
	size_t  max_count;
	size_t  bitmap_ofs;
	size_t  bitmap_len;

	// doubly linked list of pages
	struct ajmalSmallHeader  *head;
	struct ajmalSmallHeader  *tail;
};

static struct ajmalCategory ajmal_cats[NUM_CATEGORIES];


static size_t ajmal_cat_to_size[NUM_CATEGORIES] =
{
	1, 2, 4, 8,  12, 16, 24, 32,  40, 48, 56, 64
};

static size_t ajmal_size_to_cat[SMALL_OBJ_SIZE] =
{
	0, 1, 2, 2,
	3, 3, 3, 3,
	4, 4, 4, 4,
	5, 5, 5, 5,

	6, 6, 6, 6, 6, 6, 6, 6,
	7, 7, 7, 7, 7, 7, 7, 7,
	8, 8, 8, 8, 8, 8, 8, 8,
	9, 9, 9, 9, 9, 9, 9, 9,
	10, 10, 10, 10, 10, 10, 10, 10,
	11, 11, 11, 11, 11, 11, 11, 11
};


static void ajmal_init_category(int cat) {
	size_t obj_size = ajmal_cat_to_size[cat];

	ajmal_cats[cat].obj_size = obj_size;

	// determine number of objects we can store in a page.
	//
	// it is slighly tricky since the size of the bitmap area
	// depends on the number of objects, but space for objects
	// depends on the size of the bitmap.

	size_t n;
	size_t bitmap_len;
	size_t bitmap_size;

	for (n = 4096 ; ; n--) {
		bitmap_len  = ((n + 63) / 64);
		bitmap_size = bitmap_len * sizeof(uint64_t);

		if (SMALL_HEADER_SIZE + n * obj_size + bitmap_size <= 4096)
			break;
	}

	ajmal_cats[cat].max_count = n;
	ajmal_cats[cat].bitmap_ofs = 4096 - bitmap_size;
	ajmal_cats[cat].bitmap_len = bitmap_len;
}


static void ajmal_init_small_page(struct ajmalSmallHeader *hdr, int cat) {
	hdr->cat_and_sig = (cat & 0xFF) | SIGNAT_SMALL_OBJS;
	hdr->free_count = ajmal_cats[cat].max_count;

	// create the bitmap, bits are set to indicate free entries
	uintptr_t p2 = (uintptr_t)hdr;
	uint64_t *bitmap = (uint64_t *)(p2 + ajmal_cats[cat].bitmap_ofs);

	size_t bm_index;
	for (bm_index = 0 ; bm_index < ajmal_cats[cat].bitmap_len ; bm_index++) {
		bitmap[bm_index] = ~(uint64_t)0;
	}

	// link into the category, at the start
	hdr->next = ajmal_cats[cat].head;
	hdr->prev = NULL;

	if (hdr->next != NULL)
		hdr->next->prev = hdr;

	ajmal_cats[cat].head = hdr;
	if (ajmal_cats[cat].tail == NULL)
		ajmal_cats[cat].tail = hdr;
}


static void * ajmal_alloc_small_from_page(struct ajmalSmallHeader *hdr, int cat) {
	uintptr_t p2 = (uintptr_t)hdr;
	uint64_t *bitmap = (uint64_t *)(p2 + ajmal_cats[cat].bitmap_ofs);
	uint64_t *bm_end = (uint64_t *)(p2 + 4096);

	// find a free slot
	size_t index = 0;

	while (bitmap < bm_end) {
		uint64_t bits = *bitmap;

		if (bits != 0) {
			for (; (bits & 1) == 0 ; bits >>= 1) {
				index++;
			}
			// found one
			break;
		}

		bitmap++;
		index += 64;
	}

	if (index >= ajmal_cats[cat].max_count) {
		fprintf(stderr, "PANIC: aj_alloc -- corrupt page/bitmap (small)\n");
		abort();
	}

	// mark as used: clear the bit
	uint64_t bm_bit = (uint64_t)1 << (uint64_t)(index & 63);

	*bitmap &= ~bm_bit;

	hdr->free_count--;

	// compute final address
	uintptr_t offset = (uintptr_t)SMALL_HEADER_SIZE +
						(uintptr_t)index * (uintptr_t)ajmal_cats[cat].obj_size;
#if 0
	if (offset + (uintptr_t)ajmal_cats[cat].obj_size > ajmal_cats[cat].bitmap_ofs) {
		fprintf(stderr, "PANIC: small alloc overlapped the bitmap\n");
		abort();
	}
#endif

	return (void *)(p2 + offset);
}


void * ajmal_alloc_small(size_t size) {
	int cat = ajmal_size_to_cat[size - 1];

	if (ajmal_cats[cat].obj_size == 0)
		ajmal_init_category(cat);

	ajmal_stats.small_allocs++;

	// look for a free object
	struct ajmalSmallHeader *hdr;

	for (hdr = ajmal_cats[cat].head ; hdr != NULL ; hdr = hdr->next) {
		if (hdr->free_count > 0) {
			ajmal_stats.small_actual += size;

			return ajmal_alloc_small_from_page(hdr, cat);
		}
	}

	// no free objects exist, create a new page
	void *p = ajmal_os_get_pages(1);
	if (p == NULL) {
		return NULL;
	}

	hdr = (struct ajmalSmallHeader *)p;

	ajmal_init_small_page(hdr, cat);

	ajmal_stats.small_in_use += 4096;
	ajmal_stats.small_actual += size;

	return ajmal_alloc_small_from_page(hdr, cat);
}


void ajmal_free_small(size_t size, void *p) {
	int cat = ajmal_size_to_cat[size - 1];

	if (ajmal_cats[cat].obj_size == 0) {
		fprintf(stderr, "PANIC: aj_free with non-alloc'd size (small)\n");
		abort();
	}

	size_t obj_size = ajmal_cats[cat].obj_size;

	ajmal_stats.small_frees++;
	ajmal_stats.small_actual -= size;

	// get base of page, and offset within the page
	uintptr_t p2 = (uintptr_t)p;
	uintptr_t mask = ~ (uintptr_t)(4096 - 1);

	struct ajmalSmallHeader *hdr = (void *)(p2 & mask);
	size_t offset = p2 - (uintptr_t)hdr;

	// compute object index, validate it below
	size_t index = (offset - SMALL_HEADER_SIZE) / obj_size;

	// verify the header is OK.
	// [ this can cause a segfault if pointer is bad ]

	uint32_t want_sig = (cat & 0xFF) | SIGNAT_SMALL_OBJS;

	if (hdr->cat_and_sig != want_sig ||
		offset < SMALL_HEADER_SIZE ||
		index >= ajmal_cats[cat].max_count) {

		fprintf(stderr, "PANIC: aj_free with wrong size or addr (small)\n");
		abort();
	}

	// check for double free
	size_t   bm_index = (index / 64);
	uint64_t bm_bit   = (uint64_t)1 << (uint64_t)(index & 63);

	p2 = (uintptr_t)hdr;
	uint64_t *bitmap = (uint64_t *)(p2 + ajmal_cats[cat].bitmap_ofs);

	if (bitmap[bm_index] & bm_bit) {
		fprintf(stderr, "PANIC: aj_free -- double free detected (small)\n");
		abort();
	}

	// mark object as free
	bitmap[bm_index] |= bm_bit;

	hdr->free_count++;
}


#if 0  // DEBUG ONLY
void aj_print_small_bins(void) {
	printf("Small object categories:\n");

	int cat;
	for (cat = 0 ; cat < NUM_CATEGORIES ; cat++) {
		int empty = 0;
		int total = 0;

		struct ajmalSmallHeader *hdr;
		for (hdr = ajmal_cats[cat].head ; hdr != NULL ; hdr = hdr->next) {
			if (hdr->free_count == ajmal_cats[cat].max_count) {
				empty++;
			}
			total++;
		}

		printf("   size %d has %d empty pages, %d used\n",
				(int)ajmal_cats[cat].obj_size, empty, total - empty);
	}
}
#endif
