/* Copyright 2019 Andrew Apted.
 * Use of this code is governed by an MIT-style license.
 * See the accompanying "LICENSE.md" file for the full text.
 */

#include <stdlib.h>
#include <stdio.h>

#ifdef _WIN32
#include <windows.h>
#else
// Linux, *BSD, MacOS
#include <unistd.h>
#include <sys/mman.h>
#endif

#include "internal.h"


static size_t os_page_size;

static void ajmal_query_page_size(void) {
#ifdef _WIN32
	SYSTEM_INFO si;
	GetSystemInfo(&si);

	os_page_size = (size_t)si.dwPageSize;
#else
	// Linux, *BSD, MacOS
	long size = sysconf(_SC_PAGESIZE);

	if (size < 0) {
		fprintf(stderr, "PANIC: could not determine page size\n");
		abort();
	}

	os_page_size = (size_t)size;
#endif

	if (os_page_size < 4096) {
		fprintf(stderr, "PANIC: page size is too small (%d < 4096)\n", (int)os_page_size);
		abort();
	}
	if (os_page_size > 16384) {
		fprintf(stderr, "PANIC: page size is too large (%d > 16384)\n", (int)os_page_size);
		abort();
	}
}


void * ajmal_os_get_pages(size_t count) {
	if (count == 0) {
		fprintf(stderr, "PANIC: ajmal_os_get_pages with zero size\n");
		abort();
	}
	if (count >= 524288) {
		fprintf(stderr, "PANIC: ajmal_os_get_pages with extremely large size\n");
		abort();
	}

	if (os_page_size == 0) {
		ajmal_query_page_size();
	}

	// if page size is larger than 4096, scale down
	size_t factor = os_page_size / 4096;

	count = (count + factor - 1) / factor;

#ifdef _WIN32
	void *p = VirtualAlloc(NULL, count * os_page_size, MEM_RESERVE|MEM_COMMIT,
							PAGE_READWRITE);
	if (p == NULL) {
		ajmal_stats.page_alloc_errors++;
		return NULL;
	}
#else
	// Linux, *BSD, MacOS
	void *p = mmap(NULL, count * os_page_size, PROT_READ|PROT_WRITE,
					MAP_PRIVATE|MAP_ANONYMOUS, -1 /* fd */, 0 /* offset */);

	if (p == MAP_FAILED) {
		ajmal_stats.page_alloc_errors++;
		return NULL;
	}
#endif

	ajmal_stats.used_pages += count;
	ajmal_stats.total_page_allocs += count;

	return p;
}


void ajmal_os_free_pages(size_t count, void *start) {
	if (count == 0) {
		fprintf(stderr, "PANIC: ajmal_os_free_pages with zero size\n");
		abort();
	}
	if (start == NULL) {
		fprintf(stderr, "PANIC: ajmal_os_free_pages with NULL addr\n");
		abort();
	}

	if (os_page_size == 0) {
		ajmal_query_page_size();
	}

	// if page size is larger than 4096, scale down
	size_t factor = os_page_size / 4096;

	count = (count + factor - 1) / factor;

#ifdef _WIN32
	if (0 == VirtualFree(start, count * os_page_size, MEM_RELEASE)) {
		fprintf(stderr, "WARNING: VirtualFree failed in ajmal_os_free_pages\n");
		ajmal_stats.page_free_errors++;
		return;
	}
#else
	// Linux, *BSD, MacOS
	if (0 != munmap(start, count * os_page_size)) {
		fprintf(stderr, "WARNING: munmap failed in ajmal_os_free_pages\n");
		ajmal_stats.page_free_errors++;
		return;
	}
#endif

	ajmal_stats.used_pages -= count;
	ajmal_stats.total_page_frees += count;
}


void ajmal_os_print_page_size(void) {
	if (os_page_size == 0) {
		ajmal_query_page_size();
	}
	printf("Page size: %d bytes\n", (int)os_page_size);
}
