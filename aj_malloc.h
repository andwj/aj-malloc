/* Copyright 2019 Andrew Apted.
 * Use of this code is governed by an MIT-style license.
 * See the accompanying "LICENSE.md" file for the full text.
 */

#include <stddef.h>

/* aj-malloc API */

void * aj_alloc(size_t size);
void   aj_free (size_t size, void *p);
void   aj_print_mem_stats(void);

/* libc replacements */

#undef  malloc
#define malloc(s)  ajmal_c_malloc(s)
void * ajmal_c_malloc(size_t size);

#undef  free
#define free(p)  ajmal_c_free(p)
void ajmal_c_free(void *p);

#undef  calloc
#define calloc(n, s)  ajmal_c_calloc(n, s)
void * ajmal_c_calloc(size_t n, size_t size);

#undef  realloc
#define realloc(p, s)  ajmal_c_realloc(p, s)
void * ajmal_c_realloc(void *p, size_t size);

#undef  strdup
#define strdup(p)  ajmal_c_strdup(p)
char * ajmal_c_strdup(const char *p);

#undef  strndup
#define strndup(p, n)  ajmal_c_strndup(p, n)
char * ajmal_c_strndup(const char *p, size_t n);

