/* Copyright 2019 Andrew Apted.
 * Use of this code is governed by an MIT-style license.
 * See the accompanying "LICENSE.md" file for the full text.
 */

#include <stddef.h>
#include <stdint.h>

/* internal API */

#define SMALL_OBJ_SIZE   64
#define LARGE_OBJ_SIZE   262136

#define SIGNAT_USED_CHUNK    0x55730100
#define SIGNAT_FREE_CHUNK    0x46720300
#define SIGNAT_SMALL_OBJS    0x4F620500


struct ajmalStatistics {
	uint64_t used_pages;
	uint64_t total_page_allocs;
	uint64_t total_page_frees;
	uint64_t page_alloc_errors;
	uint64_t page_free_errors;

	uint64_t small_allocs;
	uint64_t small_frees;
	uint64_t small_in_use;  // will be > actual
	uint64_t small_actual;  // sum of allocs

	uint64_t medium_allocs;
	uint64_t medium_frees;
	uint64_t medium_in_use;  // will be > actual
	uint64_t medium_actual;  // sum aof allocs
	uint64_t medium_splits;
	uint64_t medium_merges;

	uint64_t large_allocs;
	uint64_t large_frees;
	uint64_t large_in_use;  // will be > actual
	uint64_t large_actual;  // sum aof allocs
};

extern struct ajmalStatistics ajmal_stats;


// allocate one or more pages from the OS.
// the returned address will be aligned to a 4096-byte boundary.
// the memory can be read and written, but cannot execute code.
// returns NULL if the allocation fails.
void * ajmal_os_get_pages(size_t count);

// free a previously allocated block of pages.
// the count value MUST be the same as used for allocation,
// and the start MUST be the result of that allocation.
// (in other words: cannot free a partial region).
void ajmal_os_free_pages(size_t count, void *start);

void ajmal_os_print_page_size(void);

// medium size allocator
void * ajmal_alloc_medium(size_t size);
void   ajmal_free_medium (size_t size, void *p);

// small object allocator
void * ajmal_alloc_small(size_t size);
void   ajmal_free_small (size_t size, void *p);
