/* Copyright 2019 Andrew Apted.
 * Use of this code is governed by an MIT-style license.
 * See the accompanying "LICENSE.md" file for the full text.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "aj_malloc.h"


/* basic test : allocate a single buffer, fill it with some
   random data, then free it.
*/


// mask must be power of two minus one
#define SIZE_MASK  ((1 << 14) - 1)
#define SIZE_ADD   1


struct {
	void *p;
	size_t size;
} allocation;


int main(void) {
	srand(time(NULL));

	size_t size = rand();
	size = (size & SIZE_MASK) + SIZE_ADD;

	size_t mul = rand() & 7;
	size = size * (mul + 1) * (mul + 1);

	void *p = aj_alloc(size);
	if (p == NULL) {
		fprintf(stderr, "allocation FAILED on %d bytes\n", (int)size);
		return -1;
	}

	fprintf(stderr, "allocated %d bytes at addr %p\n", (int)size, p);

	allocation.p = p;
	allocation.size = size;

	memset(p, rand() & 0xFF, size);

	aj_print_mem_stats();

	aj_free(allocation.size, allocation.p);
}
