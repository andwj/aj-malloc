/* Copyright 2019 Andrew Apted.
 * Use of this code is governed by an MIT-style license.
 * See the accompanying "LICENSE.md" file for the full text.
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "aj_malloc.h"


/* fuzzy test : allocate and free buffers randomly, using
   buffers in a particular size range.  Also store some data
   into each buffer (test that the memory is really there).
*/


// mask must be a power of two minus one.
// see also: 'mul' logic in the code below.
#define SIZE_MASK  ((1 << 11) - 1)
#define SIZE_ADD   1

#define NUM_STEPS  (1 << 24)

// this must be power-of-two
#define MAX_ALLOCATIONS  (1 << 12)

struct {
	void *p;
	size_t size;
} allocations[MAX_ALLOCATIONS];


void free_all(void) {
	size_t a;
	for (a = 0 ; a < MAX_ALLOCATIONS ; a++) {
		if (allocations[a].p != NULL) {
			aj_free(allocations[a].size, allocations[a].p);
		}
	}
}


int main(void) {
	srand(time(NULL));

	int i;
	for (i = 0 ; i < NUM_STEPS ; i++) {
		size_t a = rand();
		a = a & (MAX_ALLOCATIONS - 1);

		if (allocations[a].p == NULL) {
			size_t size = rand();
			size = (size & SIZE_MASK) + SIZE_ADD;

			size_t mul = rand() & 7;
			size = size * (mul + 1) * (mul + 1);

			void *p = aj_alloc(size);
			if (p == NULL) {
				fprintf(stderr, "allocation failed on %d bytes\n", (int)size);
				continue;
			}

			allocations[a].p = p;
			allocations[a].size = size;

			// store some data at random offsets in buffer
			unsigned char byte_val = (i & 0xFF);

			int k;
			for (k = 0 ; k < 23 ; k++) {
				size_t offset = rand();
				offset = offset % size;

				unsigned char *buf = (unsigned char *)p;
				buf[offset] = byte_val;
			}

		} else {
			// free existing buffer

			aj_free(allocations[a].size, allocations[a].p);
			allocations[a].p = NULL;
		}
	}

	aj_print_mem_stats();

	free_all();
}
