
MEMORY ALLOCATOR
================

This document describes a general-purpose (non-GC) memory
allocator.  It assumes a 64-bit cpu architecture, though
the same structures could be adapted to 32-bit cpus.

There are two main functions:
-  aj_alloc(size) --> pointer
-  aj_free (size, pointer)

NOTE: the free function REQUIRES the size of the object.
      when it won't be known at free time, then you MUST store
      it yourself at the beginning of the allocated block.

Internally there are three different allocators:
-  anything >  LARGE_OBJ_SIZE uses the large object allocator.
-  anything <= SMALL_OBJ_SIZE uses the small object allocator.
-  everything else uses the medium object allocator.

Currently LARGE_OBJ_SIZE is 262136, SMALL_OBJ_SIZE is 64.


LARGE OBJECT ALLOCATOR
----------------------

This allocates and frees the memory via the operating system.
For Linux and other Unices, it uses the mmap(2) and munmap(2)
syscalls.  Blocks are always freed (unmapped) immediately.


MEDIUM OBJECT ALLOCATOR
-----------------------

The medium object allocator categorizes objects into a set of
distinct sizes (listed below).  For each size there is a "bin"
which contains the free chunks of that size.  The system used
here is a "buddy allocator", larger chunks will be divided into
two smaller chunks to satisfy an allocation, and if two "buddy"
chunks get freed then they become a larger free chunk (which may
continue to be merged up the tree).

```
       72       92      120
      152      192      248
      312      392      504
      632      792     1016

     1272     1592     2040
     2552     3192     4088
     5112     6392     8184
    10232    12792    16376

    20472    25592    32760
    40952    51192    65528
    81912   102392   131064
   163832   204792   262136
```

All chunks, even allocated ones, have an hidden 8-byte header
which contains the following fields:
-  bin number (1 byte)
-  signature  (3 bytes)
-  offset from start of OS allocation   (2 bytes)
-  pages in OS allocation, usually zero (2 bytes)

Free chunks have additional fields:
-  pointer to next free chunk in the bin     (8 bytes)
-  pointer to previous free chunk in the bin (8 bytes)

The offset value in the chunk header is important for the buddy
system.  The units of the value are a number of chunks at the
current bin size.  For example, if the bin size is (1024-8),
then an offset of `3` means an absolute offset of (3 * 1024).
This scheme means we can know which chunks are buddies: the low
chunk must have an even offset, and next chunk must be odd.

To allocate a chunk, we first look in the smallest usable bin.
If not empty, then we remove its first chunk and use it.
Otherwise we look at the next power-of-two size bin, going
up to the largest bin if necessary.  If we find a free chunk,
it is split into two, moving the new chunks to smaller bins,
and possibly repeating the split process until we get the
size we originally wanted.

If all bins are empty, then we allocate some pages from the
OS and fill them with as many chunks as will fit.  This may
be a larger bin than we want, so the splitting process will
occur as described above.

To free a chunk, we add it back into the corresponding bin.
The free-list of each bin is kept in order of ascending
address.  If we detect that this chunk can be merged with a
buddy chunk (of the same size), they are merged, and the chunk
gets moved into a larger bin, which may cause further merges.

Large free chunks which were allocated from the OS may be
given back to the OS, though this is not necessarily done
immediately.  NOTE: Currently it is not done at all...


SMALL OBJECT ALLOCATOR
----------------------

The small object allocator also categorizes objects into a
set of distinct sizes (shown below).  Objects of each size
are grouped into 4096-byte pages, and each page has a bitmap
indicating which objects are in-use.  We rely on every page
having an address which is a multiple of 4096.

```
     1    2    4    8
    12   16   24   32
    40   48   56   64
```

Each page has the following structure:
-  bin number    (1 byte)
-  signature     (3 bytes)
-  free objects  (4 bytes)
-  pointer to next page     (8 bytes)
-  pointer to previous page (8 bytes)
-  the objects themselves...
-  padding
-  bitmap: ((count + 63) / 64) lots of 64-bit words

With the above structure, each page can hold the following
number of objects:

```
   3616 x 1 byte
   1916 x 2 bytes
    986 x 4 bytes
    501 x 8 bytes

    335 x 12 bytes
    252 x 16 bytes
    168 x 24 bytes
    126 x 32 bytes

    101 x 40 bytes
     84 x 48 bytes
     72 x 56 bytes
     63 x 64 bytes
```

When pages are completely free, they are able to be given
back to the OS, though this is not necessarily done
immediately.  NOTE: Currently it is not done at all...
