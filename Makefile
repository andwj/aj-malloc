
LIBRARY=libajmalloc.a

CFLAGS=-Wall -Wextra -g3 -O0

OBJS=base.o medium.o small.o os.o c_emul.o


all: $(LIBRARY) test1 test2

clean:
	rm -f $(LIBRARY) *.o ERRS test1 test2

libajmalloc.a: $(OBJS)
	ar rcv $@ $^
	ranlib $@

test1: test1.c $(LIBRARY)
	$(CC) $(CFLAGS) $^ -o $@ ./$(LIBRARY)

test2: test2.c $(LIBRARY)
	$(CC) $(CFLAGS) $^ -o $@ ./$(LIBRARY)
